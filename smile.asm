section .data
; initialize our variables
message db 'Smile!'
length equ $-message
newline db 0xa ; a new line

section .text
global _start

; no functions / subroutines here because i've forgotten lot of assembler things :D

_start:
  mov rax, 3 ; down counter
  mov rbx, 0 ; outer loop control
  mov rcx, 0 ; inner loop control

  ; start from inner loop
  jmp _inner

_outer:
  ; compare rbx with rax
  ; if equal, exit the program
  cmp rbx, rax
  je _exit

  ; store registers value to stack
  push rax
  push rbx
  push rcx
  push rdx

  ; print newline
  mov rax, 1
  mov rdi, 1
  mov rsi, newline
  mov rdx, 1
  syscall

  ; restore registers value
  pop rdx
  pop rcx
  pop rbx
  pop rax

  ; decrement rax
  ; set rcx to 0
  ; jump to inner loop
  sub rax, 1
  mov rcx, 0
  jmp _inner



_inner:
  ; compare rax with rcx
  ; if equal, stop the loop and jump to outer loop
  cmp rax, rcx
  je _outer

  ; store our register values to stack
  push rax
  push rbx
  push rcx
  push rdx

  ; print our 'Smile!' message
  mov rax, 1
  mov rdi, 1
  mov rsi, message
  mov rdx, length
  syscall

  ; restore our register values from stack
  pop rdx
  pop rcx
  pop rbx
  pop rax

  ; add rcx by 1
  add rcx, 1

  ; jump to the beginning of the inner loop
  jmp _inner

_exit:
  ; syscall exit(0)
  mov eax, 60
  xor rdi, rdi
  syscall
