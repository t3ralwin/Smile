# Smile!

Smile?

## What is this?

Answers to the question here at quora that says:

> How do I write a program that produces the following output?
>

> ```
Smile!Smile!Smile!
Smile!Smile!
Smile!
```

(Original link:  https://www.quora.com/Homework-Question-How-do-I-write-a-program-that-produces-the-following-output-1)

The simple question has been answered with a lot solutions using various programming languages with various level of complexity!

## What did you do?

I have solved the problem using these languages:

* Netwide Assembler 64 bit
* Javascript + RxJS with Node.js and ES6

Each solution produces following output:

```
Smile!Smile!Smile!
Smile!Smile!
Smile!
```
