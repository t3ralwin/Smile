'use strict';

const Rx = require(`rxjs`);
const message = `Smile!`;

function produceSequence(sequence, start, limit) {
  if ((start - limit) === 0) {
    return sequence;
  } else {
      return produceSequence(sequence.concat([start--]), start--, limit);
  }
}

function repeatString(message, n) {
  return Rx.Observable.range(0, n)
    .reduce((acc, value) => acc + message, ``);
}

Rx.Observable
  .from(produceSequence([], 3, 0))
  .switchMap(number => repeatString(message, number))
  .subscribe(console.log);